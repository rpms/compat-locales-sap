%define __filter_GLIBC_PRIVATE 1
Name:           compat-locales-sap
Version:        1.0.10
Release:        14%{?dist}
Summary:        Compatibility locales for SAP

Group:          System Environment/Libraries
License:        LGPLv2+ and GPLv2+
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0:        COPYING
Source1:        saplocales-2.2.5.tgz
Source2:        sysdep.h
Source3:        shift_jisx0213-2.5-20061008T1257.tgz
# these locales changed in glibc-2.7
Source13:       glibc-2.5-es_ES
Source14:       glibc-2.5-de_DE
Source15:       glibc-2.26-en_US
Source16:       glibc-2.26-pl_PL
Source17:       glibc-2.26-tr_TR
Source18:       glibc-2.26-cs_CZ
Source19:       glibc-2.26-sk_SK
Conflicts:      %{name} <= 1.0.10-6
Requires:       %{name}-common = %{version}-%{release}
BuildRequires:  glibc-locale-source
Patch0:         saplocales-2.2.5-rhel5.patch
Patch1:         glibc-cs_CZ@SAP.patch
Patch2:         glibc-de_DE@POSIX.patch
Patch3:         glibc-en_US@POSIX.patch
Patch4:         glibc-sk_SK@SAP.patch
Patch5:         glibc-tr_TR@SAP.patch
Patch6:         glibc-de_DE@WINDOWS.patch
Patch7:         glibc-en_US@WINDOWS.patch
Patch8:         saplocales-2.2.5-SAPSJIS.patch
Patch9:         glibc-es_ES@SAP.patch
Patch10:        glibc-en_US@Solaris.patch
Patch11:        glibc-de_DE@Solaris.patch
Patch12:        glibc-en_US@HPUX.patch
Patch13:        glibc-de_DE@HPUX.patch
Patch14:        glibc-pl_PL@HPUX.patch
Patch15:        saplocales-842448-shift-jisx0213.patch
Patch16:        glibc-cs_CZ@HPUX.patch
Patch17:        saplocales-2.2.5-rhel8.patch
Patch18:        saplocales-2.2.5-ldflags.patch

%description
Compatibility locales for legacy data on SAP Application Servers.

This package provides gconv modules for SAPSJIS and SAPSJISX0213.

%package common
Summary:        SAP locale files
Group:          System Environment/Libraries
License:        GPLv2+

%description common
This package provides various compatibility locales for use
with SAP Application Servers:

cs_CZ.ISO-8859-2@SAP, cs_CZ.ISO-8859-2@HPUX, sk_SK.ISO-8859-2@SAP,
lt_LT.ISO-8859-4@SAP, lv_LV.ISO-8859-4@SAP, et_EE.ISO-8859-4@SAP,
tr_TR.ISO-8859-9@SAP,
ja_JP.SAPSJIS, ko_KR.euckr@SAP,
de_DE.ISO-8859-1@POSIX, en_US.ISO-8859-1@POSIX,
de_DE.CP1252@WINDOWS, en_US.CP1252@WINDOWS,
es_ES.ISO-8859-1@SAP,
de_DE.ISO-8859-1@Solaris, en_US.ISO-8859-1@Solaris,
de_DE.ISO-8859-1@HPUX, en_US.ISO-8859-1@HPUX,
pl_PL.ISO-8859-1@HPUX

Note that SAPSJIS is incompatible with ASCII.

%prep
%setup -q -T -c -a1
%patch0 -p0 -b .orig
%patch17 -p1 -b .orig
%patch18 -p1 -b .orig
cp -p %SOURCE0 .
cp -p %SOURCE2 saplocales-2.2.5
cd saplocales-2.2.5
tar zxf %SOURCE3
cd ..

cp -p %SOURCE18 cs_CZ
%patch1 -p1 -b .orig
mv cs_CZ{,@SAP}

cp -p %SOURCE18 cs_CZ
%patch16 -p1 -b .orig
mv cs_CZ{,@HPUX}

cp -p %SOURCE14 de_DE
%patch2 -p1 -b .orig
mv de_DE{,@POSIX}

cp -p %SOURCE15 en_US
%patch3 -p1 -b .orig
mv en_US{,@POSIX}

cp -p %SOURCE19 sk_SK
%patch4 -p1 -b .orig
mv sk_SK{,@SAP}

cp -p %SOURCE17 tr_TR
%patch5 -p1 -b .orig
mv tr_TR{,@SAP}

cp -p %SOURCE14 de_DE
%patch6 -p1 -b .orig
mv de_DE{,@WINDOWS}

cp -p %SOURCE15 en_US
%patch7 -p1 -b .orig
mv en_US{,@WINDOWS}

# SAPSJIS fixes
%patch8 -p0 -b .orig

cp -p %SOURCE13 es_ES
%patch9 -p1 -b .orig
mv es_ES{,@SAP}

cp -p %SOURCE15 en_US
%patch10 -p1 -b .orig
mv en_US{,@Solaris}

cp -p %SOURCE14 de_DE
%patch11 -p1 -b .orig
mv de_DE{,@Solaris}

cp -p %SOURCE15 en_US
%patch12 -p1 -b .orig
mv en_US{,@HPUX}

cp -p %SOURCE14 de_DE
%patch13 -p1 -b .orig
mv de_DE{,@HPUX}

cp -p %SOURCE16 pl_PL
%patch14 -p1 -b .orig
mv pl_PL{,@HPUX}

gunzip -c %{_datadir}/i18n/charmaps/SHIFT_JISX0213.gz > saplocales-2.2.5/SAPSHIFT_JISX0213
mv saplocales-2.2.5/{shift_jisx0213.c,SAPSJISX0213.c}
%patch15 -p0 -b .orig

%build
make -C saplocales-2.2.5 gconv/SAPSJIS.so NEWFLAGS="-Wl,-z,now -DFOR_GLIBC_2_6_AND_LATER %{optflags}"
make -C saplocales-2.2.5 gconv/SAPSJISX0213.so NEWFLAGS="-Wl,-z,now -DFOR_GLIBC_2_6_AND_LATER %{optflags}"

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_libdir}
cp -a saplocales-2.2.5/gconv $RPM_BUILD_ROOT%{_libdir}

mkdir -p $RPM_BUILD_ROOT%{_datadir}/i18n/locales
cp -p cs_CZ@SAP cs_CZ@HPUX de_DE@HPUX de_DE@POSIX de_DE@Solaris de_DE@WINDOWS en_US@HPUX en_US@POSIX en_US@Solaris en_US@WINDOWS es_ES@SAP pl_PL@HPUX sk_SK@SAP tr_TR@SAP $RPM_BUILD_ROOT%{_datadir}/i18n/locales/

mkdir -p $RPM_BUILD_ROOT%{_datadir}/i18n/charmaps
cp -p saplocales-2.2.5/SAPSHIFT_JIS{,X0213} $RPM_BUILD_ROOT%{_datadir}/i18n/charmaps
mkdir  -p $RPM_BUILD_ROOT%{_prefix}/lib/locale

localedef --no-archive -ci $RPM_BUILD_ROOT%{_datadir}/i18n/locales/tr_TR@SAP \
          -f ISO-8859-9 \
          $RPM_BUILD_ROOT%{_prefix}/lib/locale/tr_TR@SAP
localedef --no-archive -ci ko_KR \
          -f EUC-KR \
          $RPM_BUILD_ROOT%{_prefix}/lib/locale/ko_KR@SAP

localedef --no-archive -ci lt_LT \
          -f ISO-8859-4 \
          $RPM_BUILD_ROOT%{_prefix}/lib/locale/lt_LT@SAP
localedef --no-archive -ci lv_LV \
          -f ISO-8859-4 \
          $RPM_BUILD_ROOT%{_prefix}/lib/locale/lv_LV@SAP
localedef --no-archive -ci et_EE \
          -f ISO-8859-4 \
          $RPM_BUILD_ROOT%{_prefix}/lib/locale/et_EE@SAP

localedef --no-archive -ci $RPM_BUILD_ROOT%{_datadir}/i18n/locales/cs_CZ@SAP \
          -f ISO-8859-2 \
          $RPM_BUILD_ROOT%{_prefix}/lib/locale/cs_CZ@SAP
localedef --no-archive -ci $RPM_BUILD_ROOT%{_datadir}/i18n/locales/cs_CZ@HPUX \
          -f ISO-8859-2 \
          $RPM_BUILD_ROOT%{_prefix}/lib/locale/cs_CZ@HPUX
localedef --no-archive -ci $RPM_BUILD_ROOT%{_datadir}/i18n/locales/sk_SK@SAP \
          -f ISO-8859-2 \
          $RPM_BUILD_ROOT%{_prefix}/lib/locale/sk_SK@SAP

localedef --no-archive -ci $RPM_BUILD_ROOT%{_datadir}/i18n/locales/de_DE@POSIX \
          -f ISO-8859-1 \
          $RPM_BUILD_ROOT%{_prefix}/lib/locale/de_DE@POSIX
localedef --no-archive -ci $RPM_BUILD_ROOT%{_datadir}/i18n/locales/en_US@POSIX \
          -f ISO-8859-1 \
          $RPM_BUILD_ROOT%{_prefix}/lib/locale/en_US@POSIX

localedef --no-archive -ci $RPM_BUILD_ROOT%{_datadir}/i18n/locales/de_DE@WINDOWS \
          -f CP1252 \
          $RPM_BUILD_ROOT%{_prefix}/lib/locale/de_DE@WINDOWS
localedef --no-archive -ci $RPM_BUILD_ROOT%{_datadir}/i18n/locales/en_US@WINDOWS \
          -f CP1252 \
          $RPM_BUILD_ROOT%{_prefix}/lib/locale/en_US@WINDOWS

# ASCII incompatible locale
localedef --no-archive -ci ja_JP \
          -f $RPM_BUILD_ROOT%{_datadir}/i18n/charmaps/SAPSHIFT_JISX0213 \
          $RPM_BUILD_ROOT%{_prefix}/lib/locale/ja_JP.SAPSJIS
localedef --no-archive -ci ja_JP \
          -f $RPM_BUILD_ROOT%{_datadir}/i18n/charmaps/SAPSHIFT_JIS \
          $RPM_BUILD_ROOT%{_prefix}/lib/locale/ja_JP.OLDSAPSJIS

localedef --no-archive -ci $RPM_BUILD_ROOT%{_datadir}/i18n/locales/es_ES@SAP \
          -f ISO-8859-1 \
          $RPM_BUILD_ROOT%{_prefix}/lib/locale/es_ES@SAP

localedef --no-archive -ci $RPM_BUILD_ROOT%{_datadir}/i18n/locales/en_US@Solaris \
          -f ISO-8859-1 \
          $RPM_BUILD_ROOT%{_prefix}/lib/locale/en_US@Solaris
localedef --no-archive -ci $RPM_BUILD_ROOT%{_datadir}/i18n/locales/de_DE@Solaris \
          -f ISO-8859-1 \
          $RPM_BUILD_ROOT%{_prefix}/lib/locale/de_DE@Solaris

localedef --no-archive -ci $RPM_BUILD_ROOT%{_datadir}/i18n/locales/en_US@HPUX \
          -f ISO-8859-1 \
          $RPM_BUILD_ROOT%{_prefix}/lib/locale/en_US@HPUX
localedef --no-archive -ci $RPM_BUILD_ROOT%{_datadir}/i18n/locales/de_DE@HPUX \
          -f ISO-8859-1 \
          $RPM_BUILD_ROOT%{_prefix}/lib/locale/de_DE@HPUX
localedef --no-archive -ci $RPM_BUILD_ROOT%{_datadir}/i18n/locales/pl_PL@HPUX \
          -f ISO-8859-2 \
          $RPM_BUILD_ROOT%{_prefix}/lib/locale/pl_PL@HPUX

gzip -n $RPM_BUILD_ROOT%{_datadir}/i18n/charmaps/SAPSHIFT_JIS*

%clean
rm -rf $RPM_BUILD_ROOT

%posttrans common
sed -i -e "/SAPSJIS/d" %{_libdir}/gconv/gconv-modules
sed -i -e "/SAPSJISX0213/d" %{_libdir}/gconv/gconv-modules

#	from			to			module		cost
cat >> %{_libdir}/gconv/gconv-modules << EOF
alias	SAPSHIFT_JIS//		SAPSJIS//
module	SAPSJIS//		INTERNAL		SAPSJIS		1
module  INTERNAL		SAPSJIS//		SAPSJIS		1

alias	SAPSHIFT_JISX0213//	SAPSJISX0213//
module	SAPSJISX0213//		INTERNAL		SAPSJISX0213	1
module  INTERNAL		SAPSJISX0213//		SAPSJISX0213	1
EOF
%{_sbindir}/iconvconfig -o %{_libdir}/gconv/gconv-modules.cache --nostdlib %{_libdir}/gconv
exit 0

%postun common
if [ $1 -eq 0 ] ; then
sed -i -e "/SAPSJIS/d" %{_libdir}/gconv/gconv-modules
sed -i -e "/SAPSJISX0213/d" %{_libdir}/gconv/gconv-modules
%{_sbindir}/iconvconfig -o %{_libdir}/gconv/gconv-modules.cache --nostdlib %{_libdir}/gconv
fi
exit 0

%files
%defattr(-,root,root,-)
%doc saplocales-2.2.5/COPYING.LIB
%{_libdir}/gconv/*

%files common
%defattr(-,root,root,-)
%doc COPYING
%{_datadir}/i18n/charmaps/*
%{_datadir}/i18n/locales/*
%dir %{_prefix}/lib/locale
%{_prefix}/lib/locale/*

%changelog
* Tue Aug 20 2019 Mike Fabian <mfabian@redhat.com> - 1.0.10-14
- glibc_post_upgrade.<_target_cpu> does not exist anymore, iconvconfig
  needs to be used in the posttrans script.
- Related: rhbz#1737442

* Thu Aug 15 2019 Mike Fabian <mfabian@redhat.com> - 1.0.10-13
- Add LD_FLAGS to really fix RPMDiff FAIL: "Not linked with -Wl,-z,now."
- Related: rhbz#1737442

* Mon Aug 05 2019 Mike Fabian <mfabian@redhat.com> - 1.0.10-12
- Fix RPMDiff FAIL: "Not linked with -Wl,-z,now."
- Resolves: rhbz#1737442

* Tue May 14 2019 Mike Fabian <mfabian@redhat.com> - 1.0.10-11
- Bump release number because the scripts for the CI tests have been updated
- Related: rhbz#1682139

* Tue May 14 2019 Mike Fabian <mfabian@redhat.com> - 1.0.10-10
- SAPSJIS and SAPSJISX0213 locales need to be part of compat-locales-sap again
- Resolves: rhbz#1691404

* Tue Apr 23 2019 Mike Fabian <mfabian@redhat.com> - 1.0.10-9
- Add Conflicts: compat-locales-sap <= 1.0.10-6 to avoid
  rpmdeplint error because of the architecture change.
- Related: rhbz#1698464

* Wed Apr 17 2019 Mike Fabian <mfabian@redhat.com> - 1.0.10-8
- Remove  BuildArch: noarch. The binary locales are architecture
  dependent.
- Related: rhbz#1698464

* Fri Apr 12 2019 Mike Fabian <mfabian@redhat.com> - 1.0.10-7
- Remove "Requires: glibc-all-langpacks" again and drop the
  posttrans and postun scripts instead. If these scripts are removed,
  glibc-all-langpacks is not needed anymore.
  This avoids problems with /usr/sbin/build-locale-archive.
- Resolves: rhbz#1698464

* Wed Apr 10 2019 Mike Fabian <mfabian@redhat.com> - 1.0.10-6
- Add "Requires: glibc-all-langpacks"
- Resolves: rhbz#1691988 (rhel-8.1.0)

* Tue Oct 16 2018 Mike Fabian <mfabian@redhat.com> - 1.0.10-5
- Bump release number
- Resolves: rhbz#1639296

* Tue Oct 16 2018 Mike Fabian <mfabian@redhat.com> - 1.0.10-5
- Move all files into the main package and drop compat-locales-sap-common
  sub-package.
- Resolves: rhbz#1639296

* Thu Feb 08 2018 Mike Fabian <mfabian@redhat.com> - 1.0.10-4
- Remove SAPSJIS and SAPSJISX0213 gconv modules as there is no
  SJIS support on RHEL8

* Tue Feb 06 2018 Mike Fabian <mfabian@redhat.com> - 1.0.10-3
- Include the sources of the locales instead of copying them from glibc

* Mon Feb 05 2018 Mike Fabian <mfabian@redhat.com> - 1.0.10-2
- Fix build for RHEL-8
- Resolves: rhbz#1523696
- add BuildRequires:  glibc-locale-source
- fix glibc-cs_CZ@SAP.patch
- fix glibc-cs_CZ@HPUX.patch
- fix glibc-sk_SK@SAP.patch
- fix glibc-tr_TR@SAP.patch
- fix category in glibc-2.5-de_DE and glibc-2.5-es_ES
- add patch for iconv stuff for RHEL-8
- add %define __filter_GLIBC_PRIVATE 1

* Wed Sep 30 2015 Mike Fabian <mfabian@redhat.com> - 1.0.10-1
- prevent locales being lost on glibc-common update (#1247865)

* Tue Sep 08 2015 Mike Fabian <mfabian@redhat.com> - 1.0.9-1
- add cs_CZ.ISO-8859-2@HPUX (#1255466)

* Thu Jan 24 2013 Jens Petersen <petersen@redhat.com> - 1.0.8-4
- change ja_JP.SAPSJIS locale from SAP SHIFT_JIS to SAP SHIFT_JISX0213
  (Takao Fujiwara, #888856)
- previous ja_JP.SAPSJIS locale is still available as ja_JP.oldsapsjis
- fix install and uninstall scripts to setup the Shift JIS gconv modules
  correctly when upgrading

* Fri Mar  9 2012 Jens Petersen <petersen@redhat.com> - 1.0.7-2
- further fixes to pl_PL.ISO-8859-2@HPUX (#784196)

* Thu Feb 23 2012 Jens Petersen <petersen@redhat.com> - 1.0.7-1
- add pl_PL.ISO-8859-2@HPUX (#784196)

* Fri Feb 18 2011 Jens Petersen <petersen@redhat.com> - 1.0.6-2
- fix collation of HPUX locales

* Mon Feb 14 2011 Jens Petersen <petersen@redhat.com> - 1.0.6-1
- add de_DE.ISO-8859-1@Solaris and en_US.ISO-8859-1@Solaris
  (Pravin Satpute, #677352)
- add de_DE.ISO-8859-1@HPUX and en_US.ISO-8859-1@HPUX
  (Pravin Satpute, #677354)

* Thu Sep  2 2010 Jens Petersen <petersen@redhat.com> - 1.0.5-1
- support es_ES.ISO-8859-1@SAP (Pravin Satpute, #629603)

* Fri Jun 25 2010 Jens Petersen <petersen@redhat.com> - 1.0.4-3
- build with debuginfo and FOR_GLIBC_2_6_AND_LATER (#607517)

* Thu Apr 29 2010 Jens Petersen <petersen@redhat.com> - 1.0.4-2
- lowercase sapsjis in postun
- prefix glibc_post_upgrade with _target_cpu instead of _arch

* Wed Apr 21 2010 Jens Petersen <petersen@redhat.com> - 1.0.4-1
- update saplocales-2.2.5-SAPSJIS.patch with SAPSHIFT_JIS fixes
  by Takao Fujiwara (#249676)

* Fri Jan 29 2010 Jens Petersen <petersen@redhat.com> - 1.0.3-15
- run glibc_post_upgrade after updating or removing

* Tue Jan 26 2010 Jens Petersen <petersen@redhat.com> - 1.0.3-14
- also use SAPSJIS// in the gconv module

* Tue Jan 26 2010 Jens Petersen <petersen@redhat.com> - 1.0.3-13
- follow glibc's naming scheme for SHIFT_JIS for SAPSHIFT_JIS (#249676):
  - alias SAPSHIFT_JIS to SAPSJIS
  - add saplocales-2.2.5-SAPSJIS.patch
- force SAPSJIS localedef like others

* Thu Jan 21 2010 Jens Petersen <petersen@redhat.com> - 1.0.3-12
- ja_JP.SAPSJIS not ja_JP.SJIS@SAP (#249676)

* Tue Jan 19 2010 Jens Petersen <petersen@redhat.com> - 1.0.3-11
- don't install gconv-modules.SAP (#249676)
- add SAPSJIS module to gconv-modules (#249676)

* Thu Jan 14 2010 Jens Petersen <petersen@redhat.com> - 1.0.3-10
- add ja_JP@SAP modified SJIS locale again with saplocales-2.2.5 (#249676)
- package is arch again

* Fri Dec 18 2009 Jens Petersen <petersen@redhat.com> - 1.0.3-9
- fix weights of ' and - in WINDOWS locales (Pravin Satpute)

* Fri Nov  6 2009 Jens Petersen <petersen@redhat.com> - 1.0.3-8
- localedef without encodings to get both names with and without defined

* Fri Oct 30 2009 Jens Petersen <petersen@redhat.com> - 1.0.3-7
- drop .CP1252 from names when defining archives of WINDOWS locales (#517889)

* Wed Oct 28 2009 Jens Petersen <petersen@redhat.com> - 1.0.3-6
- add de_DE@WINDOWS

* Wed Oct 14 2009 Jens Petersen <petersen@redhat.com> - 1.0.3-5
- fix some en_US@WINDOWS collation rules (Pravin Satpute)

* Mon Oct 12 2009 Jens Petersen <petersen@redhat.com> - 1.0.3-4
- call windows compat locale en_US.CP1252@WINDOWS

* Fri Oct  9 2009 Jens Petersen <petersen@redhat.com> - 1.0.3-3
- correct en_US@WINDOWS localedef name

* Tue Sep 22 2009 Jens Petersen <petersen@redhat.com> - 1.0.3-2
- install COPYING

* Mon Sep 14 2009 Jens Petersen <petersen@redhat.com> - 1.0.3-1
- add a en_US@WINDOWS by Pravin Satpute (#517889)
- add COPYING
- cleanup setup section

* Wed Jul 15 2009 Jens Petersen <petersen@redhat.com> - 1.0.2-1
- remove ja_JP.SJIS@SAP since it is not JISX0213: base package now empty
- make noarch

* Mon Jul  6 2009 Jens Petersen <petersen@redhat.com> - 1.0.1-1
- use patches to glibc locales rather than modified locale tarball
- update tr_TR@SAP to pass SAP tests (#467488)
- gzip SAPSHIFT_JIS

* Wed Jun 17 2009 Jens Petersen <petersen@redhat.com> - 1.0-1
- package the newer locales in a tarball and call it version 1.0 (#488915)
- update description (#488915)
- don't own gconv dir (#488915)

* Fri Jun  5 2009 Jens Petersen <petersen@redhat.com>
- drop the de_DE iso14651_HP collation for now

* Thu Jun  4 2009 Jens Petersen <petersen@redhat.com>
- rename from sap-locale to distinguish package from SAP's saplocales package
- add posix locales for de_DE and en_US (Nils Philippsen)
- add iso14651_HP collation for de_DE from OpenSuSE's sap-locale package
- update tr_TR locale with one from SuSE sap-locale
- bump version to 2.5

* Fri May 15 2009 Jens Petersen <petersen@redhat.com>
- add cs_CZ.ISO-8859-2@SAP and sk_SK.ISO-8859-2@SAP (thanks Pravin Satpute)

* Fri May  8 2009 Jens Petersen <petersen@redhat.com>
- reworked package to provide tr_TR.ISO-8859-9@SAP, ko_KR.euckr@SAP
  lt_LT.ISO-8859-4@SAP, lv_LV.ISO-8859-4@SAP, et_EE.ISO-8859-4@SAP,
  and ja_JP.SJIS@SAP (#467488)

* Fri Mar  6 2009 Jens Petersen <petersen@redhat.com>
- update README
- add some notes to common description about locale

* Mon Mar  2 2009 Jens Petersen <petersen@redhat.com>
- initial packaging of SAP package for RHEL Supplementary (#467488)
- saplocales-2.2.5-rhel5.patch with sysdep.h from Uli Drepper
