#ifdef _SYSDEP_H
#define _SYSDEP_H	1

#include <stdint.h>

#ifdef __i386__

typedef struct
{
  void *tcb;
  dtv_t *dtv;
  void *self;
  int multiple_threads;
  uintptr_t sysinfo;
  uintptr_t stack_guard;
  uintptr_t pointer_guard;
  int gscope_flag;
  int private_futex;
} tcbhead_t;

# ifdef FOR_GLIBC_2_6_AND_LATER
#  define PTR_MANGLE(var)	asm ("xorl %%gs:%c2, %0\n"		\
				     "roll $9, %0"			\
				     : "=r" (var)			\
				     : "0" (var),			\
				       "i" (offsetof (tcbhead_t,	\
						      pointer_guard)))
#  define PTR_DEMANGLE(var)	asm ("rorl $9, %0"			\
				     "xorl %%gs:%c2, %0"		\
				     : "=r" (var)			\
				     : "0" (var),			\
				       "i" (offsetof (tcbhead_t,	\
						      pointer_guard)))
# else
#  define PTR_MANGLE(var)	asm ("xorl %%gs:%c2, %0"		\
				     : "=r" (var)			\
				     : "0" (var),			\
				       "i" (offsetof (tcbhead_t,	\
						      pointer_guard)))
#  define PTR_DEMANGLE(var)	asm ("xorl %%gs:%c2, %0"		\
				     : "=r" (var)			\
				     : "0" (var),			\
				       "i" (offsetof (tcbhead_t,	\
						      pointer_guard)))
# endif

#elif defined __x86_64__

typedef struct
{
  void *tcb;
  void *dtv;
  void *self;
  int multiple_threads;
  int gscope_flag;
  uintptr_t sysinfo;
  uintptr_t stack_guard;
  uintptr_t pointer_guard;
  unsigned long int vgetcpu_cache[2];
  int private_futex;
  int __pad1;
  void *__private_tm[5];
} tcbhead_t;

# ifdef FOR_GLIBC_2_6_AND_LATER
#  define PTR_MANGLE(var)	asm ("xorq %%fs:%c2, %0\n"		\
				     "rolq $17, %0"			\
				     : "=r" (var)			\
				     : "0" (var),			\
				       "i" (offsetof (tcbhead_t,	\
						      pointer_guard)))
#  define PTR_DEMANGLE(var)	asm ("rorq $17, %0"			\
				     "xorq %%fs:%c2, %0"		\
				     : "=r" (var)			\
				     : "0" (var),			\
				       "i" (offsetof (tcbhead_t,	\
						      pointer_guard)))
# else
#  define PTR_MANGLE(var)	asm ("xorq %%fs:%c2, %0"		\
				     : "=r" (var)			\
				     : "0" (var),			\
				       "i" (offsetof (tcbhead_t,	\
						      pointer_guard)))
#  define PTR_DEMANGLE(var)	asm ("xorq %%fs:%c2, %0"		\
				     : "=r" (var)			\
				     : "0" (var),			\
				       "i" (offsetof (tcbhead_t,	\
						      pointer_guard)))
# endif

#elif defined __powerpc__ || defined __powerpc64__

typedef struct
{
  uintptr_t pointer_guard;
  uintptr_t stack_guard;
  void *dtv;
} tcbhead_t;

# ifndef __powerpc64__
register void *__thread_register __asm__ ("r2");
# else
register void *__thread_register __asm__ ("r13");
# endif

# define TLS_TCB_OFFSET 0x7000

# define THREAD_GET_POINTER_GUARD() \
    (((tcbhead_t *) ((char *) __thread_register                               \
                     - TLS_TCB_OFFSET))[-1].pointer_guard)

# define PTR_MANGLE(var) \
  (var) = (__typeof (var)) ((uintptr_t) (var) ^ THREAD_GET_POINTER_GUARD ())
# define PTR_DEMANGLE(var)     PTR_MANGLE (var)

#elif defined __IA_64__

register void *__thread_self __asm__("r13");

#define THREAD_GET_POINTER_GUARD() \
  (((uintptr_t *) __thread_self)[-2])

# define PTR_MANGLE(var) \
  (var) = (__typeof (var)) ((uintptr_t) (var) ^ THREAD_GET_POINTER_GUARD ())
# define PTR_DEMANGLE(var)     PTR_MANGLE (var)

#elif defined __s390__  || defined __s390x__

typedef struct
{
  void *tcb;
  void *dtv;
  void *self;
  int multiple_threads;
  uintptr_t sysinfo;
  uintptr_t stack_guard;
  int gscope_flag;
  int private_futex;
} tcbhead_t;

# define THREAD_SELF ((tcbhead_t *) __builtin_thread_pointer ())

#define THREAD_GETMEM(descr, member) \
  descr->member

#define THREAD_GET_POINTER_GUARD() \
  THREAD_GETMEM (THREAD_SELF, stack_guard)

# define PTR_MANGLE(var) \
  (var) = (__typeof (var)) ((uintptr_t) (var) ^ THREAD_GET_POINTER_GUARD ())
# define PTR_DEMANGLE(var)     PTR_MANGLE (var)

#else

# error "missing support for this architecture"

#endif

#endif
